# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)
from django.http import Http404
from account.models import GroupUser, LevelGroup
from documents.models import Document
from documents.serializers import DocumentSerializer
from utils.utils import QuerySetChain


class DocumentList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_document_group(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('group_id', flat=True)
            group_document = Document.objects.all(
                ).filter(group_id__in=groups_list)
            return group_document

        except GroupUser.DoesNotExist, GroupUser.AttributeError:
            raise Http404

    def get_document_level(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('group_id', flat=True)
            level_list = LevelGroup.objects.filter(
                group_id__in=groups_list).values_list('level_id', flat=True)
            group_level = Document.objects.all(
                ).filter(level_id__in=level_list)
            return group_level

        except GroupUser.DoesNotExist, GroupUser.AttributeError:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            documents_group = self.get_document_group(pk)
            documents_level = self.get_document_level(pk)
            events_total = QuerySetChain(
                documents_group, documents_level)
            serializer = DocumentSerializer(events_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)
