# -*- coding: utf-8 -*-
from django.forms import ModelForm
from suit_ckeditor.widgets import CKEditorWidget
from django import forms


class DocumentForm(ModelForm):
    class Meta:
        widgets = {
            'ck_editor': CKEditorWidget(editor_options={'startupFocus': True})
        }

    def clean(self):
        upload_or_not = self.cleaned_data.get("upload_or_not")
        if upload_or_not:
            document = self.cleaned_data.get("document")
            if not document:
                raise forms.ValidationError("No has subido documento")
