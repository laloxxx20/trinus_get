# -*- coding: utf-8 -*-
from django.apps import AppConfig


class DocumentsConfig(AppConfig):
    name = 'documents'
    verbose_name = u'Documentos'
