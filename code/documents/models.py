from django.db import models
from account.models import Group, Level


class Document(models.Model):
    group = models.ForeignKey(
        Group,
        null=True, blank=True,
        verbose_name=(u'Grupo')
    )
    level = models.ForeignKey(
        Level,
        null=True, blank=True,
        verbose_name=(u'Nivel')
    )
    title = models.CharField(
        null=False,
        blank=False,
        max_length=50,
        verbose_name=(u'Titulo')
    )
    upload_or_not = models.BooleanField(
        default=True,
        verbose_name=(u'Subir Documento'),
    )
    document = models.FileField(
        null=True, blank=True,
        upload_to='documents/%Y/%m/%d',
        verbose_name=(u'Documento'),
    )
    ck_editor = models.TextField(
        null=True, blank=True,
        verbose_name=(u'Crear documento')
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )
    readed = models.BooleanField(
        default=False,
        verbose_name=(u'Leido o no'),
    )

    class Meta:
        verbose_name_plural = 'Documentos'
        verbose_name = 'Documento'

    def _get_created_at_date(self):
        return self.created_at.date()

    def _get_full_path(self):
        if self.upload_or_not:
            return str(self.document)
        else:
            return "documents/created/"+str(self.title)+"_"+str(self.pk)+".pdf"

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.__str__())
