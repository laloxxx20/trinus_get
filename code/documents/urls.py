from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns(
    '',
    url(
        r'^documents/(?P<pk>\w+)/$',
        views.DocumentList.as_view(),
        name='documents'),
)
