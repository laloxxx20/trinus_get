# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0002_document_ck_editor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='ck_editor',
            field=models.TextField(null=True, verbose_name='Crear documento', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='document',
            field=models.FileField(upload_to=b'documents/%Y/%m/%d', null=True, verbose_name='Documentos', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='group',
            field=models.ForeignKey(verbose_name='Grupo', blank=True, to='account.Group', null=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='level',
            field=models.ForeignKey(verbose_name='Nivel', blank=True, to='account.Level', null=True),
        ),
    ]
