# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0007_auto_20151123_1614'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='readed',
            field=models.BooleanField(default=False, verbose_name='Leido o no'),
        ),
    ]
