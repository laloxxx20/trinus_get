# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('documents', '0003_auto_20151119_2009'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='upload_or_not',
            field=models.BooleanField(default=True, verbose_name='Subir Documento'),
        ),
    ]
