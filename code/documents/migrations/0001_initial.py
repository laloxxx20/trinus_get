# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_level_created_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name='Titulo')),
                ('document', models.FileField(upload_to=b'documents/%Y/%m/%d', verbose_name='Documentos')),
                ('group', models.ForeignKey(verbose_name='Grupo', to='account.Group', null=True)),
                ('level', models.ForeignKey(verbose_name='Nivel', to='account.Level', null=True)),
            ],
        ),
    ]
