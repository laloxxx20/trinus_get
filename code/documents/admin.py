# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Document
from django.contrib.admin import ModelAdmin
from .forms import DocumentForm
from django.contrib import messages
from django.conf import settings
from xhtml2pdf import pisa


class DocumentAdmin(ModelAdmin):
    class Media:
        js = (
            'document/document.js',
        )

    list_display = (
        'title',
        'created_at',
        )

    search_fields = ('title', 'created_at')

    form = DocumentForm

    def save_model(self, request, obj, form, change):
        obj.save()
        if not obj.upload_or_not:
            html_doc = obj.ck_editor
            path_to_save = settings.MEDIA_ROOT+'/documents/created/'
            html_to_pdf = open(
                                path_to_save+obj.title+'_'+str(obj.pk)+'.pdf',
                                "w+b")
            pdf_doc = pisa.CreatePDF(src=html_doc, dest=html_to_pdf)
            if pdf_doc.err:
                messages.success(
                    request,
                    ("Error al crear doc: %s") % pdf_doc.err)


admin.site.register(Document, DocumentAdmin)
