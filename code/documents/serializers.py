# -*- coding: utf-8 -*-
from rest_framework import serializers
from documents.models import Document


class DocumentSerializer(serializers.ModelSerializer):
    path = serializers.CharField(source="_get_full_path")
    created_at_date = serializers.CharField(source="_get_created_at_date")

    class Meta:
        model = Document
        fields = (
                'title',
                'path',
                'created_at_date'
                )
