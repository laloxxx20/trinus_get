# -*- coding: utf-8 -*-
from django.db import models


class Song(models.Model):
    name = models.CharField(
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Nombre de Música')
    )
    song = models.FileField(
        upload_to='songs/%Y/%m/%d',
        verbose_name=(u'Archivo de Música'),
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    class Meta:
        verbose_name_plural = 'Músicas'
        verbose_name = 'Música'

    def _get_full_path(self):
        return str(self.song)

    def _get_created_at_date(self):
        return self.created_at.date()

    def __str__(self):
        return self.name

    def __unicode__(self):
        return unicode(self.__str__())


class Meditation(models.Model):
    name = models.CharField(
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Nombre de meditación')
    )
    songs = models.ManyToManyField(Song, verbose_name=(u'Músicas'),)
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    class Meta:
        verbose_name_plural = 'Meditaciones'
        verbose_name = 'Meditación'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return unicode(self.__str__())
