# -*- coding: utf-8 -*-
from rest_framework import serializers
from meditation.models import Song


class MeditationSerializer(serializers.ModelSerializer):
    path = serializers.CharField(source="_get_full_path")
    created_at_date = serializers.CharField(source="_get_created_at_date")

    class Meta:
        model = Song
        fields = (
                'name',
                'path',
                'created_at_date',
                )
