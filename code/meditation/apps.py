# -*- coding: utf-8 -*-
from django.apps import AppConfig


class MeditationConfig(AppConfig):
    name = 'meditation'
    verbose_name = u'Meditación'
