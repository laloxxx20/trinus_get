# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meditation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, verbose_name='Nombre de M\xfasica', blank=True)),
                ('song', models.FileField(upload_to=b'songs/%Y/%m/%d')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='meditation',
            name='song',
        ),
        migrations.AddField(
            model_name='meditation',
            name='songs',
            field=models.ManyToManyField(to='meditation.Song', verbose_name='M\xfasicas'),
        ),
    ]
