# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meditation', '0003_auto_20151017_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True),
        ),
        migrations.AlterField(
            model_name='song',
            name='song',
            field=models.FileField(upload_to=b'songs/%Y/%m/%d', verbose_name='Archivo de M\xfasica'),
        ),
    ]
