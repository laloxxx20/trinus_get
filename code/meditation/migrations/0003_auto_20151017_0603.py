# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meditation', '0002_auto_20151014_1308'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='meditation',
            options={'verbose_name': 'Meditaci\xf3n', 'verbose_name_plural': 'Meditaciones'},
        ),
        migrations.AlterModelOptions(
            name='song',
            options={'verbose_name': 'M\xfasica', 'verbose_name_plural': 'M\xfasicas'},
        ),
        migrations.AlterField(
            model_name='meditation',
            name='name',
            field=models.CharField(max_length=50, null=True, verbose_name='Nombre de meditaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='song',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Archivo de M\xfasica', null=True),
        ),
        migrations.AlterField(
            model_name='song',
            name='name',
            field=models.CharField(max_length=50, null=True, verbose_name='Nombre de M\xfasica'),
        ),
        migrations.AlterField(
            model_name='song',
            name='song',
            field=models.FileField(upload_to=b'songs/%Y/%m/%d', verbose_name='Creado en'),
        ),
    ]
