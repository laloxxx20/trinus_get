from django.conf.urls import (
    patterns, url,
)
from . import views

urlpatterns = patterns(
    '',
    url(
        r'^meditations/(?P<pk>\w+)/$',
        views.MeditationList.as_view(),
        name='meditations'),
)
