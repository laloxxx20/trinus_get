# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import (
    Song,
    Meditation,
)


class MeditationAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', )
    search_fields = ('name', 'created_at', )


class SongAdmin(admin.ModelAdmin):
    list_display = ('name', 'song', 'created_at')
    search_fields = ('name', 'song', 'created_at')


admin.site.register(Meditation, MeditationAdmin)
admin.site.register(Song, SongAdmin)
