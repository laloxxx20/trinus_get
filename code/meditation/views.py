# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)
from django.http import Http404
from account.models import GroupUser, LevelGroup
from meditation.models import Song, Meditation
from .serializers import MeditationSerializer
from utils.utils import QuerySetChain


class MeditationList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_meditation_group(self, pk):
        try:
            groups_user = GroupUser.objects.select_related(
                'group__meditation').prefetch_related(
                'group__meditation__songs'
                ).filter(user=pk)
            iterables = []
            for group in groups_user:
                iterables.append(group.group.meditation.songs.all())
            return QuerySetChain(*iterables)

        except GroupUser.DoesNotExist, GroupUser.AttributeError:
            raise Http404

    def get_meditation_level(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('pk', flat=True)
            level_group = LevelGroup.objects.select_related(
                'level__meditation').prefetch_related(
                'level__meditation__songs').filter(group_id__in=groups_list)
            iterables = []
            for level in level_group:
                if level.level.meditation:
                    iterables.append(level.level.meditation.songs.all())

            return QuerySetChain(*iterables)

        except (LevelGroup.DoesNotExist, GroupUser.DoesNotExist):
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            meditations_group = self.get_meditation_group(pk)
            meditations_level = self.get_meditation_level(pk)
            songs_total = QuerySetChain(meditations_level, meditations_group)
            serializer = MeditationSerializer(songs_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)
