$(function(){
    if($('#id_upload_or_not').is(':checked')){
        console.log("checked");
        $('.field-ck_editor').hide();
    }
    else
        $('#id_document').parent().parent().parent().hide();    

    $('#id_upload_or_not').click(function(){
        if(! $('#id_upload_or_not').is(':checked')){
            $('#id_document').parent().parent().parent().hide("slow");
            $('.field-ck_editor').show("slow");
        }
        else{
            $('#id_document').parent().parent().parent().show("slow");   
            $('.field-ck_editor').hide("slow");
        }
    });
});
