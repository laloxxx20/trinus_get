# -*- coding: utf-8 -*-
import re
from django.conf import settings
from rest_framework import serializers
from django.contrib.auth.forms import SetPasswordForm
from .models import UserProfile, MyUser


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        fields = (
            'pk',
            'email',
        )


class PhoneRegex(object):
    def __init__(self, base):
        self.base = base

    def __call__(self, value):
        reg = re.compile('^\+?1?\d{9,15}$')
        if reg.match(value) is False:
            message = '''Celular debe estar conformado por el formato:
                        '+999999999'. Permitido hasta 15 digitos.'''
            raise serializers.ValidationError(message)


class UserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(source="useprofile.phone")
    cell_phone = serializers.CharField(
        source="useprofile.cell_phone",
        required=False,
    )

    class Meta:
        model = MyUser
        fields = (
                'pk',
                'email',
                'first_name',
                'last_name',
                'phone',
                'cell_phone',
                )

    def validate_phone(self, value):
        reg = re.compile(r'^\d{9,15}$')
        if reg.match(value) is None:
            message = '''Celular debe estar conformado por el formato: '999999999'. Permitido hasta 15 digitos.'''
            raise serializers.ValidationError(message)
        return value

    def validate_cell_phone(self, value):
        reg = re.compile(r'^\d{9,15}$')
        if reg.match(value) is None:
            message = '''Celular debe estar conformado por el formato: '999999999'. Permitido hasta 15 digitos.'''
            raise serializers.ValidationError(message)
        return value

    def create_or_update_profile(self, user, profile_data):
        profile, created = UserProfile.objects.get_or_create(
            user=user, defaults=profile_data)
        if not created and profile_data is not None:
            super(UserSerializer, self).update(profile, profile_data)

    # def create(self, validated_data):
    #     profile_data = validated_data.pop('userprofile', None)
    #     user = super(UserSerializer, self).create(validated_data)
    #     self.create_or_update_profile(user, profile_data)
    #     return user

    def update(self, instance, validated_data):
        profile_data = validated_data.get('useprofile', None)
        self.create_or_update_profile(instance, profile_data)
        return super(UserSerializer, self).update(instance, validated_data)


class UserPkSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        fields = ('pk',)


class UserOneSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(source='user.pk', read_only=True)
    first_name = serializers.CharField(
        source='user.first_name', read_only=True)
    last_name = serializers.CharField(
        source='user.last_name', read_only=True)
    email = serializers.CharField(
        source='user.email', read_only=True)

    class Meta:
        model = UserProfile
        fields = (
            'pk',
            'phone',
            'cell_phone',
            'first_name',
            'last_name',
            'email',
            )


class PasswordChangeSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=128)
    new_password1 = serializers.CharField(max_length=128)
    new_password2 = serializers.CharField(max_length=128)

    set_password_form_class = SetPasswordForm

    def __init__(self, *args, **kwargs):
        self.old_password_field_enabled = getattr(
            settings, 'OLD_PASSWORD_FIELD_ENABLED', False
        )
        self.user = kwargs.pop('user')
        super(PasswordChangeSerializer, self).__init__(*args, **kwargs)

        if not self.old_password_field_enabled:
            self.fields.pop('old_password')

    def validate_old_password(self, value):
        invalid_password_conditions = (
            self.old_password_field_enabled,
            self.user,
            not self.user.check_password(value)
        )

        if all(invalid_password_conditions):
            raise serializers.ValidationError('Invalid password')

        return value

    def validate(self, attrs):
        self.set_password_form = self.set_password_form_class(
            user=self.user, data=attrs
        )

        if not self.set_password_form.is_valid():
            raise serializers.ValidationError(self.set_password_form.errors)
        return attrs

    def save(self):
        self.set_password_form.save()


class PasswordRebootSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=128)

    def save(self):
        pass
