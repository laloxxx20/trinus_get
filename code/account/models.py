# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.validators import RegexValidator
from meditation.models import Meditation
from schedule.models import Calendar
# from notice.models import Notice


class MyUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name=(u'Correo Electrónico'),
        max_length=255,
        unique=True,
        blank=False,
    )
    first_name = models.CharField(
        max_length=128, verbose_name=(u'Nombres'), blank=False)
    last_name = models.CharField(
        max_length=128, verbose_name=(u'Apellidos'), blank=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    @property
    def get_full_name(self):
        # The user is identified by their email address
        return str(self.first_name + ' ' + self.first_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.last_name

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class UserProfile(models.Model):
    user = models.OneToOneField(MyUser)
    cell_phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message='''Celular debe estar conformado por el formato:
                 '+999999999'. Permitido hasta 15 digitos.''')
    cell_phone = models.CharField(
        validators=[cell_phone_regex],
        null=True,
        max_length=50,
        blank=True,
        verbose_name=(u'Celular')
        )
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message='''Teléfono debe estar conformado por el formato:
                 '+999999999'. Permitido hasta 15 digitos.''')
    phone = models.CharField(
        validators=[phone_regex],
        null=False,
        max_length=50,
        blank=False,
        verbose_name=(u'Teléfono')
        )

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return unicode(self.__str__())

    class Meta:
        verbose_name_plural = 'Datos Extra'
        verbose_name = 'Datos'


class Group(models.Model):
    name = models.CharField(
        null=False,
        max_length=50,
        blank=False,
        verbose_name=(u'Nombre')
    )
    meditation = models.ForeignKey(
        Meditation,
        verbose_name='Meditación',
        null=True,
        blank=True,
        )
    schedule = models.OneToOneField(
        Calendar,
        verbose_name='Calendario',
        null=True,
        blank=True,
        )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    class Meta:
        verbose_name_plural = 'Grupos'
        verbose_name = 'Grupo'

    def __unicode__(self):
        return self.name


class GroupUser(models.Model):
    user = models.ForeignKey(MyUser, verbose_name=(u'Usuario'))
    group = models.ForeignKey(Group, verbose_name=(u'Grupo'))

    class Meta:
        verbose_name_plural = 'Grupos de Usuario'
        verbose_name = 'Grupo'


class Level(models.Model):
    name = models.CharField(
        null=False,
        max_length=50,
        blank=False,
        verbose_name=(u'Nombre')
    )
    meditation = models.ForeignKey(
        Meditation,
        verbose_name='Meditación',
        null=True,
        blank=True,
    )
    schedule = models.OneToOneField(
        Calendar,
        verbose_name='Calendario',
        null=True,
        blank=True,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    class Meta:
        verbose_name_plural = 'Niveles'
        verbose_name = 'Nivel'

    def __unicode__(self):
        return self.name


class LevelGroup(models.Model):
    level = models.ForeignKey(Level, verbose_name=(u'Nivel'))
    group = models.ForeignKey(Group, verbose_name=(u'Grupo'))

    class Meta:
        verbose_name_plural = 'Grupos de Nivel'
        verbose_name = 'Grupo'
