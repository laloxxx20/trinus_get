# -*- coding: utf-8 -*-
from .forms import (
    UserProfileForm,
    UserCreationForm,
    UserChangeForm,
    LevelFormRestriction,
)
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from easy_select2 import select2_modelform
from models import (
    MyUser,
    GroupUser,
    Group,
    Level,
    UserProfile,
    LevelGroup,
)


class UserProfileInline(admin.TabularInline):
    model = UserProfile
    formset = UserProfileForm

GroupUserForm = select2_modelform(GroupUser, attrs={'width': '220px'})


class GroupUserInline(admin.TabularInline):
    form = GroupUserForm
    model = GroupUser


class MyUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    inlines = [UserProfileInline, GroupUserInline]
    search_fields = ('email', 'first_name', 'last_name', )

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'first_name', 'last_name', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Información Personal', {'fields': ('first_name', 'last_name',)}),
        ('Permisos', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': (
                    'email', 'first_name', 'last_name',
                    'password1', 'password2')
            }
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

LevelGroupForm = select2_modelform(LevelGroup, attrs={'width': '220px'})
LevelForm = select2_modelform(Level, attrs={'width': '220px'})


class LevelGroupInline(admin.TabularInline):
    form = LevelGroupForm
    model = LevelGroup


class LevelAdmin(admin.ModelAdmin):
    form = LevelFormRestriction
    list_display = ('name', 'created_at', )
    inlines = [LevelGroupInline, ]
    search_fields = ('name', 'created_at', )


GroupForm = select2_modelform(Group, attrs={'width': '220px'})


class GroupAdmin(admin.ModelAdmin):
    form = GroupForm
    list_display = ('name', 'created_at', )
    search_fields = ('name', 'created_at', )

admin.site.register(MyUser, MyUserAdmin)
admin.site.register(Level, LevelAdmin)
admin.site.register(Group, GroupAdmin)
