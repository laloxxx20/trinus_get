from django.conf.urls import (
    patterns, url,
    include,
)
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'list_users', views.UserListViewSet)

urlpatterns = patterns(
    '',
    url(r'^', include(router.urls)),
    url(r'^users/$', views.UserList.as_view(), name='users'),
    url(
        r'^userpk/(?P<email>[\w@.]+)/$',
        views.UserPkView.as_view(), name='user_pk'),
    url(r'^users/(?P<pk>\w+)/$', views.UserList.as_view(), name='user'),
    url(
        r'^change_pass/(?P<pk>\w+)/$',
        views.PasswordChangeView.as_view(),
        name='change_pass'),
)
