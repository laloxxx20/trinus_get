# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_auto_20151029_2245'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='levelgroup',
            options={'verbose_name': 'Grupo', 'verbose_name_plural': 'Grupos de Nivel'},
        ),
        migrations.AddField(
            model_name='group',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True),
        ),
    ]
