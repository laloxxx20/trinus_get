# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20151016_0124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='groupuser',
            name='group',
            field=models.ForeignKey(verbose_name='Grupo', to='account.Group'),
        ),
        migrations.AlterField(
            model_name='groupuser',
            name='user',
            field=models.ForeignKey(verbose_name='Usuario', to=settings.AUTH_USER_MODEL),
        ),
    ]
