# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_auto_20151029_2339'),
    ]

    operations = [
        migrations.AddField(
            model_name='level',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True),
        ),
    ]
