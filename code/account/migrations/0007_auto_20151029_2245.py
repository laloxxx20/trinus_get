# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20151029_2242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='schedule',
            field=models.OneToOneField(null=True, blank=True, to='schedule.Calendar', verbose_name=b'Calendario'),
        ),
        migrations.AlterField(
            model_name='level',
            name='schedule',
            field=models.OneToOneField(null=True, blank=True, to='schedule.Calendar', verbose_name=b'Calendario'),
        ),
    ]
