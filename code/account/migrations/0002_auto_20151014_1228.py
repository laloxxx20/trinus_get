# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meditation', '0001_initial'),
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='meditation',
            field=models.ForeignKey(verbose_name=b'Meditaci\xc3\xb3n', to='meditation.Meditation', null=True),
        ),
        migrations.AddField(
            model_name='level',
            name='meditation',
            field=models.ForeignKey(verbose_name=b'Meditaci\xc3\xb3n', to='meditation.Meditation', null=True),
        ),
    ]
