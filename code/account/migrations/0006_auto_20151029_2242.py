# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20151029_2234'),
        ('account', '0005_auto_20151028_0548'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='schedule',
            field=models.ForeignKey(verbose_name=b'Calendario', blank=True, to='schedule.Calendar', null=True),
        ),
        migrations.AddField(
            model_name='level',
            name='schedule',
            field=models.ForeignKey(verbose_name=b'Calendario', blank=True, to='schedule.Calendar', null=True),
        ),
    ]
