# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20151014_1228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='meditation',
            field=models.ForeignKey(verbose_name=b'Meditaci\xc3\xb3n', blank=True, to='meditation.Meditation', null=True),
        ),
        migrations.AlterField(
            model_name='level',
            name='meditation',
            field=models.ForeignKey(verbose_name=b'Meditaci\xc3\xb3n', blank=True, to='meditation.Meditation', null=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='cell_phone',
            field=models.CharField(max_length=50, null=True, verbose_name='Celular', blank=True),
        ),
    ]
