# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_auto_20151017_0523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='cell_phone',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Celular', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Celular debe estar conformado por el formato:\n                 '+999999999'. Permitido hasta 15 digitos.")]),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='phone',
            field=models.CharField(max_length=50, verbose_name='Tel\xe9fono', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Tel\xc3\xa9fono debe estar conformado por el formato:\n                 '+999999999'. Permitido hasta 15 digitos.")]),
        ),
    ]
