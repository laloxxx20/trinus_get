# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from .serializers import (
    UserListSerializer,
    UserSerializer,
    UserOneSerializer,
    PasswordChangeSerializer,
    UserPkSerializer,
)
from models import MyUser, UserProfile
from rest_framework import permissions, viewsets
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
    # TokenHasScope,
)


class UserList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_object_profile(self, pk):
        try:
            return UserProfile.objects.get(user=pk)
        except UserProfile.DoesNotExist:
            raise Http404

    def get_object_user(self, pk):
        try:
            return MyUser.objects.get(pk=pk)
        except MyUser.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            user = self.get_object_profile(pk)
            serializer = UserOneSerializer(user)
        else:
            raise Http404
        return Response(serializer.data)

    def put(self, request, pk=None, format=None):
        if pk:
            user = self.get_object_user(pk)
            serializer = UserSerializer(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            raise Http404


class UserListViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = MyUser.objects.all()
    serializer_class = UserListSerializer


class UserPkView(APIView):
    def get_object(self, email):
        try:
            return MyUser.objects.get(email=email)
        except MyUser.DoesNotExist:
            raise Http404

    def get(self, request, email=None, format=None):
        if email:
            user = self.get_object(email)
            serializer = UserPkSerializer(user)
        else:
            raise Http404
        return Response(serializer.data)


class PasswordChangeView(GenericAPIView):

    """
    Calls Django Auth SetPasswordForm save method.
    Accepts the following POST parameters: new_password1, new_password2
    Returns the success/fail message.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object_user(self, pk):
        try:
            return MyUser.objects.get(pk=pk)
        except MyUser.DoesNotExist:
            raise Http404

    def put(self, request, pk=None):
        user = self.get_object_user(pk)
        serializer = PasswordChangeSerializer(user=user, data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        serializer.save()
        return Response({"success": "Nueva Contraseña fue guardada."})


class PasswordReboot(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def put(seld, request):
        serializer = PasswordRebootSerializer(data=request.data)