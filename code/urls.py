"""trinus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = [
    url(r'', include(admin.site.urls)),
    # url(r'^auth/', include('rest_auth.urls')),
    url(r'^', include('account.urls', namespace="account")),
    url(r'^', include('meditation.urls', namespace="medtation")),
    url(r'^', include('schedule.urls', namespace="schedule")),
    url(r'^', include('notice.urls', namespace="notice")),
    url(r'^', include('video.urls', namespace="video")),
    url(r'^', include('documents.urls', namespace="document")),
    url(r'^', include('goal.urls', namespace="goal")),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^media/(?P<path>.*)$',
        'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
]
