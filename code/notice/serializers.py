# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import NoticesPool
from push_notifications.models import APNSDevice, GCMDevice


class NoticeSerializer(serializers.ModelSerializer):
    created_at_date = serializers.CharField(
        source="notices._get_created_at_date")
    title = serializers.CharField(
        source="notices.title")
    notice = serializers.CharField(
        source="notices.notice")

    class Meta:
        model = NoticesPool
        fields = (
                'pk',
                'title',
                'notice',
                'created_at_date',
                'state',
                )


class NoticeStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoticesPool
        fields = ('state',)

    def update_state(self, notice):
            NoticesPool.objects.filter(pk=notice.pk).update(
                state=self.data.get('state'))


class APNSDeviceSerializer(serializers.ModelSerializer):
    # device_id = serializers.UUIDField(required=True, format='hex_verbose')

    class Meta:
        model = APNSDevice
        fields = ('device_id', 'registration_id')

    def create_or_update_profile(self, pk_user, profile_data):
        profile, created = APNSDevice.objects.get_or_create(
            user_id=pk_user.pk,
            defaults=profile_data)
        if not created and profile_data is not None:
            super(APNSDeviceSerializer, self).update(profile, profile_data)

    def update(self, instance, validated_data):
        self.create_or_update_profile(instance, validated_data)
        return super(APNSDeviceSerializer, self).update(
            instance, validated_data)


class GCMDeviceSerializer(serializers.ModelSerializer):
    # device_id = serializers.UUIDField(required=True, format='hex_verbose')

    class Meta:
        model = GCMDevice
        fields = ('device_id', 'registration_id')

    def create_or_update_profile(self, pk_user, profile_data):
        profile, created = GCMDevice.objects.get_or_create(
            user_id=pk_user.pk,
            defaults=profile_data)
        if not created and profile_data is not None:
            super(GCMDeviceSerializer, self).update(profile, profile_data)

    def update(self, instance, validated_data):
        self.create_or_update_profile(instance, validated_data)
        return super(GCMDeviceSerializer, self).update(
            instance, validated_data)
