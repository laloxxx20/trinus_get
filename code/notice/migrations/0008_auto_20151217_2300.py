# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0007_noticeswhom'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticeswhom',
            name='gruop',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='noticeswhom',
            name='level',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='noticeswhom',
            name='user',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
