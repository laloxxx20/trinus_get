# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0002_notice_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='state',
            field=models.PositiveIntegerField(default=0, verbose_name='Estado', choices=[(0, b'No leido'), (1, b'Leido')]),
        ),
    ]
