# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0010_auto_20151217_2330'),
    ]

    operations = [
        migrations.RenameField(
            model_name='noticeswhom',
            old_name='gruop',
            new_name='group',
        ),
    ]
