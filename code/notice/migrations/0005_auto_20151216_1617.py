# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0004_auto_20151215_1716'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='noticespool',
            name='group',
        ),
        migrations.RemoveField(
            model_name='noticespool',
            name='level',
        ),
        migrations.AlterField(
            model_name='noticespool',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
