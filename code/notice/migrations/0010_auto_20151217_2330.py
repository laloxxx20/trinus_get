# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0009_notice_notices_whom'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='notice',
        #     name='notices_whom',
        # ),
        migrations.AddField(
            model_name='noticeswhom',
            name='notice',
            field=models.ForeignKey(to='notice.Notice'),
        ),
    ]
