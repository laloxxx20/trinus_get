# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='notice',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True),
        ),
    ]
