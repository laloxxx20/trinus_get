# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_level_created_at'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, null=True, verbose_name='Titulo de la Noticia')),
                ('notice', models.TextField(verbose_name='Noticia')),
                ('group', models.ForeignKey(verbose_name=b'Grupo', blank=True, to='account.Group', null=True)),
                ('level', models.ForeignKey(verbose_name=b'Nivel', blank=True, to='account.Level', null=True)),
                ('user', models.ForeignKey(verbose_name=b'Usuario', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Noticia',
                'verbose_name_plural': 'Noticias',
            },
        ),
    ]
