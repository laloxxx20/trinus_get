# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_level_created_at'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('notice', '0003_notice_state'),
    ]

    operations = [
        migrations.CreateModel(
            name='NoticesPool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group', models.ForeignKey(verbose_name=b'Grupo', blank=True, to='account.Group', null=True)),
                ('level', models.ForeignKey(verbose_name=b'Nivel', blank=True, to='account.Level', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='notice',
            name='group',
        ),
        migrations.RemoveField(
            model_name='notice',
            name='level',
        ),
        migrations.RemoveField(
            model_name='notice',
            name='state',
        ),
        migrations.RemoveField(
            model_name='notice',
            name='user',
        ),
        migrations.AddField(
            model_name='noticespool',
            name='notices',
            field=models.ForeignKey(to='notice.Notice'),
        ),
        migrations.AddField(
            model_name='noticespool',
            name='user',
            field=models.ForeignKey(verbose_name=b'Usuario', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
