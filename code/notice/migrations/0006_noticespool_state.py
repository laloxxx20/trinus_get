# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notice', '0005_auto_20151216_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticespool',
            name='state',
            field=models.PositiveIntegerField(default=0, choices=[(0, b'No leido'), (1, b'Leido')]),
        ),
    ]
