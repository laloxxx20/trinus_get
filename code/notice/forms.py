# -*- coding: utf-8 -*-
from django import forms
from account.models import (
    Level,
    Group,
    MyUser,
)
from .models import Notice, NoticesWhom
from django.core.exceptions import ValidationError


class ChoicesForm(forms.ModelForm):

    grupo = forms.ModelChoiceField(
                                    queryset=Group.objects.all(),
                                    required=False)
    nivel = forms.ModelChoiceField(
                                    queryset=Level.objects.all(),
                                    required=False)
    usuario = forms.ModelChoiceField(
                                    queryset=MyUser.objects.all(),
                                    required=False)

    def __init__(self, *args, **kwargs):
        super(ChoicesForm, self).__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        if instance:
            whom = NoticesWhom.objects.filter(notice=instance).first()
            if whom.user:
                self.initial['usuario'] = whom.user
            if whom.group:
                self.initial['grupo'] = whom.group
            if whom.level:
                self.initial['nivel'] = whom.level

    def clean(self):
        user = self.cleaned_data['usuario']
        group = self.cleaned_data['grupo']
        level = self.cleaned_data['nivel']

        count = 0
        if user:
            count += 1
        if group:
            count += 1
        if level:
            count += 1
        if count == 0:
            raise ValidationError(
                """Debe ingresar por lo menos una opcion de grupo, nivel o
                usuario.""")
        return self.cleaned_data

    class Meta:
        model = Notice
        exclude = ('created_at',)
