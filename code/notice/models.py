from django.db import models
from account.models import MyUser


class Notice(models.Model):
    title = models.CharField(
       null=True,
       max_length=50,
       blank=False,
       verbose_name=(u'Titulo de la Noticia')
    )
    notice = models.TextField(
        verbose_name=(u'Noticia')
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    class Meta:
        verbose_name_plural = 'Noticias'
        verbose_name = 'Noticia'

    def _get_created_at_date(self):
        return self.created_at.date()

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return unicode(self.__str__())


class NoticesWhom(models.Model):
    notice = models.ForeignKey(Notice)
    user = models.PositiveIntegerField(null=True)
    group = models.PositiveIntegerField(null=True)
    level = models.PositiveIntegerField(null=True)


class NoticesPool(models.Model):
    user = models.ForeignKey(MyUser)
    notices = models.ForeignKey(Notice)
    state = models.PositiveIntegerField(
        choices=(
            (0, "No leido"),
            (1, "Leido"),
            ),
        default=0,
    )
