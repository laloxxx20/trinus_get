# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)
from rest_framework import status
from django.http import Http404
from push_notifications.models import APNSDevice, GCMDevice
from account.models import MyUser
from notice.models import Notice, NoticesPool
from notice.serializers import (
    NoticeSerializer,
    NoticeStateSerializer,
    APNSDeviceSerializer,
    GCMDeviceSerializer
)


class NoticeList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get(self, request, pk=None, format=None):
        if pk:
            try:
                notices_total = NoticesPool.objects.filter(user=pk)
            except NoticesPool.DoesNotExist:
                raise Http404

            serializer = NoticeSerializer(notices_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)

    def get_notice(self, pk_notice):
        try:
            return NoticesPool.objects.get(pk=pk_notice)
        except Notice.DoesNotExist:
            raise Http404

    def put(self, request, pk_notice=None, format=None):
        if pk_notice:
            notice = self.get_notice(pk_notice)
            serializer = NoticeStateSerializer(data=request.data)
            if serializer.is_valid():
                serializer.update_state(notice)
                return Response(serializer.data)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            raise Http404


class Notifications(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def put(self, request, type_token, pk_user, format=None):
        try:
            user = MyUser.objects.filter(pk=pk_user).first()
        except MyUser.DoesNotExist:
            raise Http404

        if type_token == "apn":
            serializer = APNSDeviceSerializer(user, data=request.data)
        if type_token == "gcm":
            serializer = GCMDeviceSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, type_token, pk, format=None):
        try:
            if type_token == "apn":
                APNSDevice.objects.get(user_id=pk).delete()
            if type_token == "gcm":
                GCMDevice.objects.get(user_id=pk).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except APNSDevice.DoesNotExist, GCMDevice.DoesNotExist:
            raise Http404
