from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns(
    '',
    url(
        r'^notices/(?P<pk>\w+)/$',
        views.NoticeList.as_view(),
        name='notices'),
    url(
        r'^notices/state/(?P<pk_notice>\w+)/$',
        views.NoticeList.as_view(),
        name='notice_state'),
    url(
        r'^notifications/create_token/(?P<type_token>\w+)/(?P<pk_user>\w+)/$',
        views.Notifications.as_view(),
        name='notifications'),
    url(
        r'^notifications/delete_token/(?P<type_token>\w+)/(?P<pk>\w+)/$',
        views.Notifications.as_view(),
        name='delete_notifications'),
)
