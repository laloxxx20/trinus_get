# -*- coding: utf-8 -*-
from django.apps import AppConfig


class NoticeConfig(AppConfig):
    name = 'notice'
    verbose_name = u'Noticia'
