from django.contrib import admin
from .models import (
    Notice,
)
from easy_select2 import select2_modelform
from .forms import ChoicesForm
from account.models import (
    GroupUser,
    LevelGroup,
)
from .models import NoticesPool, NoticesWhom
from push_notifications.models import APNSDevice, GCMDevice
from push_notifications import NotificationError

NoticeForm = select2_modelform(
                Notice,
                attrs={'width': '220px'},
                form_class=ChoicesForm
            )


class NoticeAdmin(admin.ModelAdmin):
    form = NoticeForm
    list_display = ('title', 'created_at')
    search_fields = ('title', 'created_at')

    def send_notices(self, users_pk, text):
        devices = GCMDevice.objects.filter(user__in=users_pk)
        try:
            devices.send_message(text, extra={"notice_pk": 1})
        except NotificationError(Exception):
            raise NotificationError(Exception)

        devices = APNSDevice.objects.filter(user__in=users_pk)
        try:
            devices.send_message(text, extra={"notice_pk": 1})
        except NotificationError(Exception):
            raise NotificationError(Exception)

    def save_model(self, request, obj, form, change):
        instance = form.save()
        user = form.cleaned_data.get('usuario', None)
        group = form.cleaned_data.get('grupo', None)
        level = form.cleaned_data.get('nivel', None)
        title = form.cleaned_data.get('title', None)
        noticeWhom = {}
        list_to_send = []
        create_list = []

        if user:
            list_to_send.append(user.pk)
            noticeWhom['user'] = user.pk
        if group:
            users = GroupUser.objects.filter(group=group)
            for user in users:
                list_to_send.append(user.user.pk)
            noticeWhom['group'] = group.pk

        if level:
            groups = LevelGroup.objects.filter(
                level=level).values_list('group', flat=True)
            users = GroupUser.objects.filter(group__in=groups)
            for user in users:
                list_to_send.append(user.user.pk)
            noticeWhom['level'] = level.pk

        list_to_send = list(set(list_to_send))
        self.send_notices(list_to_send, title)

        create_list = []
        for user in list_to_send:
            create_list.append(NoticesPool(
                    user_id=user, notices=instance))
        NoticesPool.objects.bulk_create(create_list)

        obj, created = NoticesWhom.objects.update_or_create(
                notice=instance,
                defaults=noticeWhom)


admin.site.register(Notice, NoticeAdmin)
