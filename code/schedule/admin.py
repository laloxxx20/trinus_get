# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import TextInput, Textarea
from .models import (
    Calendar,
    Event,
    Calls,
)


class EventInline(admin.TabularInline):
    model = Event
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={
                            'style': 'width: 80%;'})},
        models.TextField: {'widget': Textarea(attrs={
                            'style': 'height: 80%; width: 70%;'})},
    }


class CallsInline(admin.TabularInline):
    model = Calls


class CalendarAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at')
    inlines = [
        EventInline,
        CallsInline,
    ]

    search_fields = ('name', 'created_at')


admin.site.register(Calendar, CalendarAdmin)
# admin.site.register(Event)
