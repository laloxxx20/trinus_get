from django.conf.urls import (
    patterns, url,
)
from . import views

urlpatterns = patterns(
    '',
    url(
        r'^calendar/(?P<pk>\w+)/$',
        views.CalendarView.as_view(),
        name='calendar'),
    url(
        r'^calls/(?P<pk>\w+)/$',
        views.CallsList.as_view(),
        name='calls'),
)
