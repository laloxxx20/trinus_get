# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import RegexValidator
from colorfield.fields import ColorField


class Calendar(models.Model):
    name = models.CharField(
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Nombre de Calendario')
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return unicode(self.__str__())

    class Meta:
        verbose_name_plural = 'Calendarios'
        verbose_name = 'Calendario'


class Event(models.Model):
    calendar = models.ForeignKey(Calendar, verbose_name=(u'Calendario'))
    title = models.CharField(
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Título')
    )
    description = models.TextField(
        null=True,
        blank=False,
        verbose_name=(u'Descripción')
    )
    day = models.DateField(
        null=True, blank=False,
        verbose_name=(u'Fecha'),
    )
    time_start = models.TimeField(
        null=False, blank=False,
        verbose_name=(u'Tiempo inicio'),
    )
    time_end = models.TimeField(
        null=True, blank=True,
        verbose_name=(u'Tiempo fin'),
    )
    color = ColorField(
        verbose_name=(u'Color'),
        null=True,
        blank=False,
        default='',
    )

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.__str__())

    class Meta:
        verbose_name_plural = 'Eventos'
        verbose_name = 'Evento'


class Calls(models.Model):
    calendar = models.ForeignKey(Calendar, verbose_name=(u'Calendario'))
    title = models.CharField(
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Título')
    )
    time_start = models.DateTimeField(
        null=False, blank=False,
        verbose_name=(u'Tiempo inicio'),
    )
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message='''Teléfono debe estar conformado por el formato:
                 '+999999999'. Permitido hasta 15 digitos y min 9.''')
    phone = models.CharField(
        validators=[phone_regex],
        null=True,
        max_length=50,
        blank=False,
        verbose_name=(u'Teléfono')
        )

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.__str__())

    class Meta:
        verbose_name_plural = 'Llamadas'
        verbose_name = 'Llamada'
