# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)
from django.http import Http404
from account.models import GroupUser, LevelGroup, Group
from .models import Event, Calls
from .serializers import CalendarSerializer, CallsSerializer
from utils.utils import QuerySetChain


class CalendarView(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_calendar_group(self, pk):
        try:
            groups_user = GroupUser.objects.select_related(
                'group__schedule'
                ).filter(user=pk).values_list('group__schedule', flat=True)
            return Event.objects.filter(calendar__in=groups_user)

        except (GroupUser.DoesNotExist, GroupUser.AttributeError,
                Event.DoesNotExist):
            raise Http404

    def get_calendar_level(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('pk', flat=True)
            levels_group = LevelGroup.objects.select_related(
                'level__schedule'
                ).filter(
                group_id__in=groups_list).values_list(
                    'level__schedule', flat=True)
            return Event.objects.filter(calendar__in=levels_group)

        except (LevelGroup.DoesNotExist, LevelGroup.AttributeError,
                GroupUser.DoesNotExist, GroupUser.AttributeError,
                Event.DoesNotExist):
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            calendar_group = self.get_calendar_group(pk)
            calendar_level = self.get_calendar_level(pk)
            print "calendar_group: ", calendar_group
            print "calendar_level: ", calendar_level
            events_total = QuerySetChain(calendar_group, calendar_level)
            serializer = CalendarSerializer(events_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)


class CallsList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_calls_group(self, pk):
        try:
            groups_user = GroupUser.objects.select_related(
                'group__schedule'
                ).filter(user=pk).values_list('group', flat=True)
            return Calls.objects.filter(calendar__in=groups_user)

        except (GroupUser.DoesNotExist, GroupUser.AttributeError,
                Calls.DoesNotExist):
            raise Http404

    def get_calls_level(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('pk', flat=True)
            levels_group = LevelGroup.objects.select_related(
                'level__schedule'
                ).filter(
                group_id__in=groups_list).values_list('level', flat=True)
            return Calls.objects.filter(calendar__in=levels_group)

        except (LevelGroup.DoesNotExist, LevelGroup.AttributeError,
                GroupUser.DoesNotExist, GroupUser.AttributeError,
                Calls.DoesNotExist):
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            calls_group = self.get_calls_group(pk)
            calls_level = self.get_calls_level(pk)
            calls_total = QuerySetChain(calls_group, calls_level)
            serializer = CallsSerializer(calls_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)
