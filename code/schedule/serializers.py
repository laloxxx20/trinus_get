# -*- coding: utf-8 -*-
from rest_framework import serializers
from schedule.models import Event, Calls


class CalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
                'title',
                'description',
                'day',
                'time_start',
                'time_end',
                'color',
                )


class CallsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Calls
        fields = (
                'title',
                'time_start',
                'phone',
                )
