# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0006_auto_20151030_0141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='color',
            field=colorfield.fields.ColorField(default=b'', max_length=10, null=True, verbose_name='Color'),
        ),
    ]
