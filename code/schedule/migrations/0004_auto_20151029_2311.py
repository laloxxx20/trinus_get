# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20151029_2234'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='day',
            field=models.DateField(null=True, verbose_name='Tiempo inicio'),
        ),
        migrations.AlterField(
            model_name='event',
            name='time_end',
            field=models.TimeField(null=True, verbose_name='Tiempo fin', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='time_start',
            field=models.TimeField(verbose_name='Tiempo inicio'),
        ),
    ]
