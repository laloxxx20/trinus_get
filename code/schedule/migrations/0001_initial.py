# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, verbose_name='Nombre de Calendario')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Calls',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, null=True, verbose_name='T\xedtulo')),
                ('time_start', models.DateTimeField(verbose_name='Tiempo inicio')),
                ('calendar', models.ForeignKey(verbose_name='Calendario', to='schedule.Calendar')),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(null=True, verbose_name='Descripci\xf3n')),
                ('time_start', models.DateTimeField(verbose_name='Tiempo inicio')),
                ('time_end', models.DateTimeField(null=True, verbose_name='Tiempo fin', blank=True)),
                ('title', models.CharField(max_length=50, null=True, verbose_name='Color')),
                ('calendar', models.ForeignKey(verbose_name='Calendario', to='schedule.Calendar')),
            ],
        ),
    ]
