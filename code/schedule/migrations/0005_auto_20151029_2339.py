# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0004_auto_20151029_2311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calls',
            name='phone',
            field=models.CharField(max_length=50, null=True, verbose_name='Tel\xe9fono', validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Tel\xc3\xa9fono debe estar conformado por el formato:\n                 '+999999999'. Permitido hasta 15 digitos y min 9.")]),
        ),
        migrations.AlterField(
            model_name='event',
            name='day',
            field=models.DateField(null=True, verbose_name='Fecha'),
        ),
    ]
