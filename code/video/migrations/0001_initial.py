# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_level_created_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, null=True, verbose_name='T\xedtulo del Video')),
                ('description', models.TextField(verbose_name='Descripci\xf3n')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True)),
                ('videourl', models.FileField(upload_to=b'videos/%Y/%m/%d', verbose_name='Archivo de Video')),
                ('group', models.ForeignKey(verbose_name=b'Grupo', blank=True, to='account.Group', null=True)),
                ('level', models.ForeignKey(verbose_name=b'Nivel', blank=True, to='account.Level', null=True)),
            ],
            options={
                'verbose_name': 'Video',
                'verbose_name_plural': 'Videos',
            },
        ),
    ]
