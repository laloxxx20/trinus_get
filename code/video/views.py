# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)

from django.http import Http404
from account.models import GroupUser, LevelGroup
from video.models import Video
from video.serializers import VideoSerializer
from utils.utils import QuerySetChain


class VideoList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_video_group(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('group_id', flat=True)
            group_video = Video.objects.all(
                ).filter(group_id__in=groups_list)
            return group_video

        except GroupUser.DoesNotExist, GroupUser.AttributeError:
            raise Http404

    def get_video_level(self, pk):
        try:
            groups_list = GroupUser.objects.filter(
                    user=pk).values_list('group_id', flat=True)
            level_list = LevelGroup.objects.filter(
                group_id__in=groups_list).values_list('level_id', flat=True)
            group_level = Video.objects.all(
                ).filter(level_id__in=level_list)
            return group_level

        except GroupUser.DoesNotExist, GroupUser.AttributeError:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            videos_group = self.get_video_group(pk)
            videos_level = self.get_video_level(pk)
            videos_total = QuerySetChain(
                videos_group, videos_level)
            serializer = VideoSerializer(videos_total, many=True)
        else:
            raise Http404
        return Response(serializer.data)
