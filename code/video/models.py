# -*- coding: utf-8 -*-
from django.db import models
from account.models import Group
from account.models import Level

# Create your models here.


class Video(models.Model):
    title = models.CharField(
       null=True,
       max_length=50,
       blank=False,
       verbose_name=(u'Título del Video')
    )
    description = models.TextField(
        verbose_name=(u'Descripción')
    )
    level = models.ForeignKey(
        Level,
        verbose_name='Nivel',
        null=True,
        blank=True,
    )
    group = models.ForeignKey(
       Group,
       verbose_name='Grupo',
       null=True,
       blank=True,
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        verbose_name=(u'Creado en'),
    )
    videourl = models.FileField(
        upload_to='videos/%Y/%m/%d',
        verbose_name=(u'Archivo de Video'),
    )

    class Meta:
        verbose_name_plural = 'Videos'
        verbose_name = 'Video'

    def _get_created_at_date(self):
        return self.created_at.date()

    def _get_full_path(self):
        return str(self.videourl)

    def __str__(self):
        return str(self.title)

    def __unicode__(self):
        return unicode(self.__str__())
