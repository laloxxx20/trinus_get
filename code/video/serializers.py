# -*- coding: utf-8 -*-
from rest_framework import serializers
from video.models import Video


class VideoSerializer(serializers.ModelSerializer):
    created_at_date = serializers.CharField(source="_get_created_at_date")

    class Meta:
        model = Video
        fields = (
                'title',
                'description',
                'videourl',
                'created_at_date',
                )
