from django.conf.urls import (
    patterns, url,
)
from . import views

urlpatterns = patterns(
    '',
    url(
        r'^videos/(?P<pk>\w+)/$',
        views.VideoList.as_view(),
        name='videos'),
)
