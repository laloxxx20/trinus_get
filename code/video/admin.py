from django.contrib import admin
from .models import (
    Video,
)
from easy_select2 import select2_modelform
# Register your models here.


VideoForm = select2_modelform(Video, attrs={'width': '220px'})


class VideoAdmin(admin.ModelAdmin):
    form = VideoForm
    list_display = ('title', 'videourl', 'created_at')
    search_fields = ('title', 'videourl', 'created_at')


admin.site.register(Video, VideoAdmin)
