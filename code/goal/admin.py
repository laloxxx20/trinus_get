from django.contrib import admin
from .models import (
    Goal,
)
# Register your models here.


class GoalAdmin(admin.ModelAdmin):
    list_display = (
        'action',
        'full_name',
        # 'user',
        'state',
        'created_at'
    )
    search_fields = (
        'action',
        'user__email',
        'user__first_name',
        'user__last_name',
        'state',
        'created_at',
    )
    fields = (
        'user',
        'action',
        'state',
        'created_at',
        )

    def full_name(self, obj):
        return obj.user.get_full_name
    full_name.short_description = 'Nombre'
    full_name.admin_order_field = 'user__last_name'

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = []
        for field in self.model._meta.fields:
            readonly_fields.append(field.name)
        return readonly_fields

admin.site.register(Goal, GoalAdmin)
