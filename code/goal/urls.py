from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns(
    '',
    url(r'^goals/(?P<pk>\w+)/$', views.GoalList.as_view(), name='goals'),
    url(
        r'^goals/delete/(?P<pk_user>\w+)/$',
        views.GoalList.as_view(),
        name='goals_delete'),
    url(
        r'^goals/state/(?P<pk_goal>\w+)/$',
        views.GoalList.as_view(),
        name='goal_state'),
)
