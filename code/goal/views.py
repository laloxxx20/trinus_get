# -*- coding: utf-8 -*-
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from oauth2_provider.ext.rest_framework import (
    TokenHasReadWriteScope,
)
from rest_framework import status
from django.http import Http404
from goal.models import Goal
from account.models import MyUser
from .serializers import (
    GoalSerializer,
    UserGoalSerializer,
    GoalUpdateSerializer
)


class GoalList(APIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]

    def get_goal_user(self, pk):
        try:
            return Goal.objects.filter(user=pk)
        except Goal.DoesNotExist:
            raise Http404

    def get_object_user(self, pk):
        try:
            return MyUser.objects.get(pk=pk)
        except MyUser.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            goal_user = self.get_goal_user(pk)
            serializer = GoalSerializer(goal_user, many=True)
        else:
            raise Http404
        return Response(serializer.data)

    def get_goal(self, pk_goal):
        try:
            return Goal.objects.get(pk=pk_goal)
        except Goal.DoesNotExist:
            raise Http404

    def put(self, request, pk=None, pk_goal=None, format=None):
        if pk:
            user = self.get_object_user(pk)
            serializer = UserGoalSerializer(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif pk_goal:
            goal = self.get_goal(pk_goal)
            serializer = GoalUpdateSerializer(data=request.data)
            if serializer.is_valid():
                serializer.update_state(goal)
                return Response(serializer.data)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            raise Http404

    def delete(self, request, pk_user, format=None):
        try:
            Goal.objects.filter(user=pk_user).delete()
            return Response(
                status=status.HTTP_204_NO_CONTENT)
        except Goal.DoesNotExist:
            raise Http404
