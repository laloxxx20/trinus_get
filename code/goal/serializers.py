# -*- coding: utf-8 -*-
from rest_framework import serializers
from goal.models import Goal


class GoalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Goal
        fields = (
                'pk',
                'action',
                # 'date',
                'state',
                )


class UserGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = (
                'pk',
                'action',
                'date',
                'state',
                )

    def update(self, instance, validated_data):
        Goal.objects.create(
            user=instance, action=validated_data.get('action'))
        return super(UserGoalSerializer, self).update(instance, validated_data)


class GoalUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Goal
        fields = (
                'state',
                )

    def update_state(self, goal):
        Goal.objects.filter(pk=goal.pk).update(
            state=self.data.get('state'))
