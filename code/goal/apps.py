# -*- coding: utf-8 -*-
from django.apps import AppConfig


class GoalConfig(AppConfig):
    name = 'goal'
    verbose_name = u'Metas'
