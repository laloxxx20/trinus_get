# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goal', '0003_auto_20151215_1307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='state',
            field=models.CharField(default=b'incomplete', max_length=10, verbose_name='Estado de meta', choices=[(b'incomplete', b'Incompleto'), (b'complete', b'Completo')]),
        ),
    ]
