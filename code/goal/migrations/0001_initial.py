# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Goal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('action', models.TextField(verbose_name='Acci\xf3n')),
                ('date', models.DateTimeField(null=True, verbose_name='Fecha')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Creado en', null=True)),
                ('state', models.CharField(default=b'incomplete', max_length=10, verbose_name='Estado', choices=[(b'incomplete', b'Incompleto'), (b'complete', b'Completo')])),
            ],
            options={
                'verbose_name': 'Meta',
                'verbose_name_plural': 'Metas',
            },
        ),
    ]
