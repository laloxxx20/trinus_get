# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goal', '0002_auto_20151214_1627'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goal',
            name='action',
            field=models.TextField(max_length=500, verbose_name='Acci\xf3n'),
        ),
    ]
