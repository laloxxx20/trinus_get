# -*- coding: utf-8 -*-
from django.db import models
from account.models import MyUser

# Create your models here.


class Goal(models.Model):
    user = models.ForeignKey(MyUser, verbose_name=(u'Usuario'), default=None)
    action = models.TextField(
        verbose_name=(u'Acción'),
        max_length=500,
    )
    date = models.DateTimeField(
        null=True,
        verbose_name=(u'Fecha'),
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name=(u'Creado en'),
    )
    state = models.CharField(
        max_length=10,
        choices=(
            ('incomplete', "Incompleto"),
            ('complete', "Completo"),
            ),
        default='incomplete',
        verbose_name=(u'Estado de meta'),
    )

    class Meta:
        verbose_name_plural = 'Metas'
        verbose_name = 'Meta'

    def _get_created_at_date(self):
        return self.created_at.date()

    def __str__(self):
        return str(self.action)

    def __unicode__(self):
        return unicode(self.__str__())
